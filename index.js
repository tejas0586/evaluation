"use strict";

const closeBtn = document.querySelector(".close-btn"); // For card click
const closingBtn = document.querySelector(".closing-btn"); //For button click
const modal = document.getElementById("newModal"); // For card click
const newModal = document.getElementById("newerModal"); //For button click
const cards = document.querySelectorAll(".col");
const btns = document.querySelectorAll(".btn");
const currentVal = document.querySelector(".current-val");
const slider = document.getElementById("slider");
const prices = document.getElementsByClassName("card");
const priceHeaders = document.getElementsByClassName("card-header");

// Showing modal
cards.forEach((card) =>
	card.addEventListener("click", function (e) {
		e.stopPropagation();
		modal.showModal();
	})
);
btns.forEach((btn) =>
	btn.addEventListener("click", function (e) {
		e.stopPropagation();
		newModal.showModal();
	})
);
// Closing modal
closeBtn.addEventListener("click", function (e) {
	e.stopPropagation();
	modal.close();
});
closingBtn.addEventListener("click", function (e) {
	e.stopPropagation();
	newModal.close();
});
//closing modal if clicked outside
window.onclick = function (e) {
	if (e.target === modal) {
		modal.close();
	}
};
window.onclick = function (e) {
	if (e.target === newModal) {
		newModal.close();
	}
};

slider.addEventListener("input", function (e) {
	let val = e.target.value;
	currentVal.innerHTML = val;
	for (let i = 0; i < 3; i++) {
		prices[i].classList.remove("border-primary");
		priceHeaders[i].classList.remove("text-bg-primary");
		priceHeaders[i].classList.remove("border-primary");
	}
	if (val <= 10) {
		priceHeaders[0].classList.add("text-bg-primary");
		priceHeaders[0].classList.add("border-primary");
		prices[0].classList.add("border-primary");
	} else if (val > 10 && val <= 20) {
		priceHeaders[1].classList.add("text-bg-primary");
		priceHeaders[1].classList.add("border-primary");
		prices[1].classList.add("border-primary");
	} else {
		priceHeaders[2].classList.add("text-bg-primary");
		priceHeaders[2].classList.add("border-primary");
		prices[2].classList.add("border-primary");
	}
});
